/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacionprobabilidad;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import org.jfree.data.xy.XYSeries;

/**
 *
 * @author Byron
 */
public class SimulacionProbabilidad extends Application{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        XYSeries datosReales = new XYSeries("Casos nuevos");
        XYSeries datos = new XYSeries("Casos nuevos (real)");
        //Variable independiente
        int[] x = new int[92];
        //Variable dependiente
        int[] y = {1,0,0,2,0,6,0,7,18,11,12,8,37,26,30,52,25,71,72,92,21,48,69,94,96,108,159,96,106,139,79,94,201,274,169,250,236,67,76,127,126,128,206,182,171,185,172,207,205,320,261,237,218,352,262,296,499,279,383,305,640,346,497,595,444,568,550,659,658,680,606,723,635,721,640,752,643,801,1046,998,806,1022,1101,1262,1322,1548,1147,1110,1340,1521,1766,1515};
        /**
         * Promedio de casos nuevos por día
         */
        int py = 0;
        
        for (int i = 0; i < 92; i++) {
            py = py +y[i];
        }
        py = py/92;
        System.out.println(py);
        /**
         * Y en el modelo probabilístico
         */
        int[] yp = new int[93];
        yp[0] = y[91];
        for (int i = 1; i < 57; i++) {
            yp[i] = yp[i-1]+py;
        }
        
        for (int i = 1; i < 93; i++) {
            datos.add(i, yp[i-1]);
            datosReales.add(i,y[i-1]);
        }
        
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Días");
        yAxis.setLabel("Casos confirmados");
        //creating the chart
        final LineChart<Number,Number> lineChart = 
                new LineChart<Number,Number>(xAxis,yAxis);
        lineChart.setTitle("Gráfica de los casos confirmados en Colombia");
        //defining a datosSerie
        XYChart.Series datosSerie = new XYChart.Series();
        XYChart.Series datosRealesSerie = new XYChart.Series();
        datosSerie.setName("Casos confirmados en Colombia (Predicción probabilística)");
        datosRealesSerie.setName("Casos confirmados en Colombia (Realidad)");
        //populating the datosSerie with data
        
        for (int i = 1; i < 93; i++) {
                datosRealesSerie.getData().add(new XYChart.Data(i,y[i-1]));
            }
        
        datosSerie.getData().add(new XYChart.Data(92, yp[0]));
        datosSerie.getData().add(new XYChart.Data(120, yp[26]));
        
        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().add(datosSerie);
        lineChart.getData().add(datosRealesSerie);
       
        stage.setScene(scene);
        stage.setTitle("Casos confirmados en Colombia");
        stage.show();
        }
}
